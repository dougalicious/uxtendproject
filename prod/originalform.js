// function PPTTMZ(date, time, tmz1, tmz2) {
//   Logger.log(`${date}, ${time}, ${tmz1}, ${tmz2}`);
//   const isBlank = (str) => str.toString() === ""
//   if([date, time, tmz2].some( isBlank)){
//      return null
//   }
//   let getGMTofTMZ = (tmzAsStr) => {
//     let tmzRegex = /GMT(-|\+)\d+:\d+/gi.exec(tmzAsStr)
//     return tmzRegex === null ? tmzAsStr : tmzRegex[0]
//   }
//   const getFirstTimeMatch = (str) => {
//     try {
//       let firstTimeFound = str.match(/(\d+:\d+\s?\w+)/gi)[0]
//       return firstTimeFound
//     } catch (e) { return str }
//   }
//   Logger.log('about to convert')
//   return _convertDateTimeToPPT({time, date, tmz: tmz2})

//   function _convertDateTimeToPPT({ time, date, tmz }) {
//     try {
//       date = Utilities.formatDate(new Date(date), getGMTofTMZ(tmz1), "MM/dd/yyyy")
//       let regexTime = getFirstTimeMatch(time)

//       let gmtOfTmz = getGMTofTMZ(tmz)
//       let dateObjWithTmz = new Date(`${date} ${regexTime} ${gmtOfTmz}`);
//       Logger.log(`date, regextime gmtoftmz ${date} ${regexTime} ${gmtOfTmz}`)
//       Logger.log(`dateObjWithTmz: ${dateObjWithTmz}`)
//       // tmz: "GMT-8:00" is hardcoded b/c variable tmz is pulled infrom ss where its only has standard time, and therefore on the heatmap to repersent PST daylight saving isn't accounted for
//       let isValidDate = dateObjWithTmz.toString() !== "Invalid Date"
//       let dateWithTmzAsStr = isValidDate ? Utilities.formatDate(dateObjWithTmz, getGMTofTMZ(tmz1), "MM/dd/yyyy_h:mm a") : [date, time].join("_")
//       let [dateConvert, timeConvert] = dateWithTmzAsStr.split('_');
//       Logger.log(`dateWithTmzAsStr: ${dateWithTmzAsStr}`)
//       return timeConvert
//     } catch (e) {
//       Logger.log(`convert time error: ${time}, ${date}`)
//       return `please try again tmz must have GMT-/+#, =PPTTMZ(date, time, uxr_tmz, ppt_tmz) `
//     }
//   }
// }

// function tmztest(){
//   let [date, time, tmz1, tmz2] =
//   [
//     "Fri Jun 04 2021 00:00:00 GMT-0400 (GMT-04:00)", "3:00 AM", "EAT - Eastern African Time (GMT+3:00)", "MIT - Midway Islands Time (GMT-11:00)",
//   ]

//   	// {"0":"2021-06-04T04:00:00.000Z","1":"11:00","2":"EAT - Eastern African Time (GMT+3:00)","3":"EST - Eastern Standard Time (GMT-5:00)"}
//     let props = date.name()
//   let ans = PPTTMZ(date,time, tmz1, tmz2);
//   debugger
// }