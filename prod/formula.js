function PPTTMZ(date, time, tmz1, tmz2) {
  Logger.log(`${date}, ${time}, ${tmz1}, ${tmz2}`);
  const isBlank = (str) => str.toString() === ""
  if([date, time, tmz2].some( isBlank)){
     return null
  }
  let getGMTofTMZ = (tmzAsStr) => {
    let tmzRegex = /GMT(-|\+)\d+:\d+/gi.exec(tmzAsStr)
    return tmzRegex === null ? tmzAsStr : tmzRegex[0]
  }
  const getFirstTimeMatch = (str) => {
    try {
      let firstTimeFound = str.match(/(\d+:\d+\s?\w+)/gi)[0]
      return firstTimeFound
    } catch (e) { return str }
  }
  Logger.log('about to convert')
  return _convertDateTimeToPPT({time, date, tmz: tmz1})

  function _convertDateTimeToPPT({ time, date, tmz }) {
    try {
      date = Utilities.formatDate(new Date(date), getGMTofTMZ(tmz2), "MM/dd/yyyy")
      let regexTime = getFirstTimeMatch(time)

      let gmtOfTmz = getGMTofTMZ(tmz)
      let dateObjWithTmz = new Date(`${date} ${regexTime} ${gmtOfTmz}`);
      Logger.log(`date, regextime gmtoftmz ${date} ${regexTime} ${gmtOfTmz}`)
      Logger.log(`dateObjWithTmz: ${dateObjWithTmz}`)
      // tmz: "GMT-8:00" is hardcoded b/c variable tmz is pulled infrom ss where its only has standard time, and therefore on the heatmap to repersent PST daylight saving isn't accounted for
      let isValidDate = dateObjWithTmz.toString() !== "Invalid Date"
      let dateWithTmzAsStr = isValidDate ? Utilities.formatDate(dateObjWithTmz, getGMTofTMZ(tmz2), "MM/dd/yyyy_h:mm a") : [date, time].join("_")
      let [dateConvert, timeConvert] = dateWithTmzAsStr.split('_');
      Logger.log(`dateWithTmzAsStr: ${dateWithTmzAsStr}`)
      return timeConvert
    } catch (e) {
      Logger.log(`convert time error: ${time}, ${date}`)
      return `please try again tmz must have GMT-/+#, =PPTTMZ(date, time, uxr_tmz, ppt_tmz) `
    }
  }
}

