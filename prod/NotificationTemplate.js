function fetchClaimStudyHTMLTemplate(props, sheetName) {
  const colHeaders = Object.keys(props)
  const {
    "Study spreadsheet": studySpreadsheetLink,
    "UXtend member": uxr,
    "RC": rc,
    "How many participants do you need for this study?": numPPTS,
    "What's the name of the study?": studyName,
    "What is the first date of the study?": targetStartDate,
    "Which type of study are you planning to do?": studyType,
    "UXR location": uxrLocation,
    "Product area": productArea,
    "Pod": pod

  } = props
  Logger.log(Object.keys(props))
  debugger
  return fetchClaimStudyTemplate(`
  <body>
  	Hello ${uxr},
    <br><br>
  	<p>You have been assigned a new <a href="${studySpreadsheetLink}">study request</a> [link to study request].
  	<br>Here are the full request details:</p>
  	<ul>
  		<li>[UXR: ${props["Email Address"]}] - Column M on ${sheetName} Tab</li>
  		<li>[RC: ${rc}] - Column Q on ${sheetName} Tab</li>
  		<li>[Number of PPTs: ${numPPTS}] - Column AA on ${sheetName} Tab</li>
  		<li>[Study Name: ${studyName}] - Column G on Active Studies Tab</li>
  		<li>[Target Start Date: ${targetStartDate}] - Column F on ${sheetName} Tab</li>
  		<li>[Study Type: ${studyType}] - Column Y on ${sheetName} Tab</li>
  		<li>[Study Spreadsheet: <a href="${studySpreadsheetLink}">Link</a>] - Column K on ${sheetName} Tab</li>
  		<li>[Researcher location: ${uxrLocation}] - Column W on ${sheetName} Tab</li>
  		<li>[Product Area: ${productArea}] - Column x on ${sheetName} Tab</li>
  		<li>[Pod Supporting Study: ${pod}] - Column U on ${sheetName} Tab</li>
  	</ul>
    <br><br>
  	Thank you!
  	Super duper allocator script Ninja
  
  </body>
`)

  function fetchClaimStudyTemplate(body){
    return `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=4.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"><link rel="stylesheet" href=""><title>{{title}}</title></head><style>body{font-family:Roboto,Arial,Helvetica,sans-serif;font-size:15px;color:#000;-webkit-text-size-adjust:none !important;-webkit-font-smoothing:antialiased !important;-ms-text-size-adjust:none !important}table,tr,td{mso-table-lspace:0pt;mso-table-rspace:0pt}a:link,a:visited,a:hover,a:active{color:#4285f4;text-decoration:none}.appleLinks a{color:#000 !important;text-decoration:none !important}strong{font-weight:bold !important}em{font-style:italic !important}.yshortcuts a span{color:inherit !important;border-bottom:none !important}html{-webkit-text-size-adjust:none;-ms-text-size-adjust:100%}.ReadMsgbody1{width:100%}.ExternalClass{width:100%}.ExternalClass *{line-height:100%}td{-webkit-text-size-adjust:none}a[href^=tel]{color:inherit;text-decoration:none}.mob-hide{display:none !important}div,p,a,li,td{-webkit-text-size-adjust:none}td{text-decoration:none !important}a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important}@media screen and (max-width:480px){.pd{padding-left:20px !important;padding-right:20px !important}}h3{margin-top:0;margin-bottom:1}#table{border-collapse:collapse}#th,#td{padding:10px;text-align:left;border:1px solid #efefef}</style><body style="margin:10 !important; padding:0px 0 0 0px !important; background-color:#FFFFFF;"><table width="480" style="max-width: 600px; width: 100%; margin: 0 auto; text-align: center;" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top" align="center" style="padding-top: 10"><table border="0" align="center" cellpadding="0" cellspacing="10" width="100%"><tr><td class="leftColumnContent"> <img src="http://services.google.com/fh/files/emails/ux_infra_logo_long.png" class="columnImage" width="230" alt="logoImageUrl" style="-ms-interpolation-mode:bicubic; height:auto; outline:none; text-decoration:none" height="auto"></td></tr></table><tr><td align="center" valign="top" bgcolor="#ffffff" style="-webkit-border-radius:0px; -moz-border-radius:0px; border-radius: 0px; border-left: 1px solid #efefef; border-top: 1px solid #efefef; border-right: 1px solid #efefef; border-bottom: 1px solid #efefef;"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top" align="left" style="font-family:Roboto, Helvetica, Arial sans-serif; font-size: 14px; line-height:24px; color: #414347; padding:30px 40px 30px 40px;">${body}</tbody></table></td></tr></table></body></html>`
}
}
