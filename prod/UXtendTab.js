const DATA = {
  SheetNames: {
    Schedule: {
      name: "Schedule",
      SessionHeader: "Session Details",
      FilterHeader: "Legal Name",
      PassThroughHeaderName: {
        // { sessionDay, sessionTime, "Timezone": tmz, "Participant Status": participatStatus, "Legal Name": name, "Email": email, "Phone Number": phoneNumber, "Detailed Feedback": notes, ...args }
        tmz: "Timezone",
        participantStatus: "Participant Type",
        name: "Legal Name",
        email: "Email",
        phoneNumber: "Phone Number",
        notes: "Detailed FeedBack"
      }
    },
    DayOfStudy: {
      name: "Day-Of Study",
      headerStartIdx: 2,
      dataStartIdx: 3,
      rangeIdxDataStart: 4
    },
    UXtend: {
      Template: { templateId: "1ZpTgyuR0EFBLWiK-0mEyvq-BtYu0ikvkWfUa30EQf9U", sheetName: "PptTmz Day-Of Study" },
      TimeRange: "B:B",
      TimeNumberFormat: "@STRING@",
      PPTTMZ: "C4:C23"
    }

  }
}


function onEvent(e) {
  return {
    isSheet: (name) => e.source.getActiveSheet().getName() === name
  }
}
// schedule tab update 
function onEditUXtend(e) {
  const hasDayOfStudyTab = e.source.getSheetByName(DATA.SheetNames.DayOfStudy.name)
  const isScheduleTab = onEvent(e).isSheet(DATA.SheetNames.Schedule.name)
  try {
    Logger.log(e.source.getActiveSheet().getName())
    Logger.log(`hasDayOfStudyTab  ${hasDayOfStudyTab}`)
    Logger.log(`isScheduleTab  ${isScheduleTab}`)
    hasDayOfStudyTab && isScheduleTab && updateUXtendTab(e.source, e.range)
  } catch (e) {
    throw (e)
  }
  Logger.log("finshed onedituxtend")
}
function onEditDayOfStudy(e) {
  Logger.log('onedit day of study')
  try {
    const hasDayOfStudyTab = e.source.getSheetByName(DATA.SheetNames.DayOfStudy.name)
    const isScheduleTab = onEvent(e).isSheet(DATA.SheetNames.DayOfStudy.name)
    hasDayOfStudyTab && isScheduleTab && updateBackgroundDayOfStudy(hasDayOfStudyTab)
  } catch (e) {
    throw (e)
  }

}
function updateBackgroundDayOfStudy(sheet) {
  const getValues = (props) => Object.values(props)
  let { body } = getDataBySheet(sheet, 2, 3)
  body.map(getValues).forEach((_props, idx, arr) => applyBackGroundColor(sheet, idx, arr))
}


function updateUXtendTab(activeSpreadsheet, eventRange) {
  Logger.log(`updating Day-Of Study sheet`)
  //https://docs.google.com/spreadsheets/d/1PHMuyib_uR0l5z6A9qj7M7OzomTnzz_0EnyHxrDNqOM/edit#gid=829970003
  // let id = "1Ru4tVTaep9Xp1d75IGtmQi9LzvRoGTVQxDIHb5vMCgc"
  // id = "1PHMuyib_uR0l5z6A9qj7M7OzomTnzz_0EnyHxrDNqOM"
  // let id = "1eToNC_r42kyOzU3Mlcw3PHMkrnjkEcIJjZvxL8e6FFM"
  // Utilities.sleep(15000)
  activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.openById(id)
  const hasSheetNameBySpreadsheet = (ss) => (sheetName) => ss.getSheetByName(sheetName)
  activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.getActiveSpreadsheet();
  let isDateObj = (prop) => prop instanceof Date
  let isDateSame = (date1, date2) => date1.getTime() == date2.getTime()
  let isDateObjects = (...args) => args.every(isDateObj)

  const currDayOfStudy = getDayOfStudyCurrentSessions(activeSpreadsheet)
  Utilities.sleep(5000)
  const uxTendTab = hasSheetNameBySpreadsheet(activeSpreadsheet)(DATA.SheetNames.DayOfStudy.name)
  const uxTendSheetHeadersAsObj = currDayOfStudy ? getHeadersAsObj({ sheet: uxTendTab, headerIdx: 3 }) : null
  const sessionData = fetchSessionsFromScheduleTab({ scheduleTab: fetchScheduleTab(activeSpreadsheet) })
  // Logger.log(`uxtendSheetHeaderAsObj k: ${JSON.stringify(Object.entries(uxTendSheetHeadersAsObj))}`)

  const next = sessionData.map(sessionToDayOf(uxTendSheetHeadersAsObj));
  

  const dayOfStudyTab = refreshUXtend(currDayOfStudy, next, uxTendTab, eventRange)
  updateBackgroundDayOfStudy(dayOfStudyTab)

  function refreshUXtend(curr, next, uxTendTab, eventRange) {
    const insertRowToSheet = ({ sheet, idx, values, props }) => {
      sheet = sheet.insertRowBefore(idx)
      return { sheet, idx: idx, values, props }
    }
    const insertValuesToSheet = ({ sheet, idx, values, props }) => {
      Utilities.sleep(500)
      const uxTendSheetHeadersAsObj = getHeadersAsObj({ sheet, headerIdx: 3 })
      Object.keys(props).forEach((str, valueIdx) => {
        let headerIdx = uxTendSheetHeadersAsObj.hasOwnProperty(str) ? uxTendSheetHeadersAsObj[str] !== "" ? uxTendSheetHeadersAsObj[str] : null : null
        headerIdx !== null ? sheet.getRange(idx, headerIdx + 1).setValue([props[str]]) : null
      })
    }
    const matchParams = ["time", "date", "legal Name"]

    // let headerMatchType = ["Legal Name", "Email", "Phone Number"];
    let headerMatchType = ["Legal Name", "Day", "Time"];
    let newData = findNewData(curr, next, uxTendTab, eventRange);
    let hasNewData = newData.length !== 0
    const updateDayOfStudySheet = compose(insertValuesToSheet, insertRowToSheet);
    SpreadsheetApp.flush()
    const updateSheetWithNewData = () => newData.forEach(({ uxtendTab, props, rowIdx }) => updateDayOfStudySheet({ sheet: uxtendTab, props, values: Object.values(props), idx: rowIdx + 1 }))
    hasNewData ? updateSheetWithNewData() : updateCurrentRows(curr, next, eventRange)


    /** before stuff */
    // debugger
    // if (curr.length === next.length) {
    //   updateCurrentRows(curr, next, eventRange)
    // } else if (next.length > curr.length) {
    //   let newRows = findNewData(curr, next, uxTendTab, eventRange);
    //   debugger
    //   // const updateDayOfStudySheet = compose(insertValuesToSheet, insertRowToSheet)
    //   newRows.forEach(({ uxtendTab, props, rowIdx }) => updateDayOfStudySheet({ sheet: uxtendTab, props, values: Object.values(props), idx: rowIdx + 1 }))
    // } else if (next.length < curr.length) {
    // shouldn't happen
    // createUXtendTab(activeSpreadsheet)
    // }
    
    scheduleChanges(curr, next)
    return uxTendTab;
    function findNewData(curr, next, uxtendTab, evenRange) {
      
      let newData = next.filter(function (nextProps, idx) {

        let newRowAsObj = curr.find((currentProps, cIdx) => {
          return headerMatchType.every(key => currentProps[key].toString().toLowerCase() === nextProps[key].toString().toLowerCase())
        }) ? false : true

        return newRowAsObj
      })

      // determines which row to insert and add data on "Day-Of Study" sheet
      // get idx of next +3 for header on "Day-Of Study"
      const getRowIdxOfUpdate = (props) => next.findIndex(nextProps => Object.keys(nextProps).slice(1).every(key => props[key].toString() === nextProps[key].toString())) + 3
      const getRowIdxOfUpdateByHeaderMatchType = (props) => next.findIndex(nextProps => headerMatchType.every(key => props[key].toString() === nextProps[key].toString())) + 3
      let temp = newData.map(props => Object.assign({}, { props, uxtendTab, rowIdx: getRowIdxOfUpdateByHeaderMatchType(props) }))
      let temp2 = newData.map(props => Object.assign({}, { props, uxtendTab, rowIdx: getRowIdxOfUpdate(props) }))
      return temp
      return newData.map(props => Object.assign({}, { props, uxtendTab, rowIdx: getRowIdxOfUpdate(props) }))
    }
    // removed data from schedule removes from "day-of study"
    function scheduleChanges(dayOfData, scheduleData) {
      Logger.log("inside scheduleChanges")
      
      // const findNonAssociatedDayOf = (dayOfProps) => scheduleData.some(scheduleProp => headerMatchType.every(header => scheduleProp[header] === dayOfProps[header])) ? false : true
      const findNonAssociatedDayOf = (dayOfProps) => scheduleData.some(scheduleProp => headerMatchType.every(header => {
        let fromSchedule = scheduleProp[header]
        let fromDayOf = dayOfProps[header]
        return fromSchedule.toString() === fromDayOf.toString()
      })) ? false : true
      const nonAssociatedDayOf = dayOfData.filter(findNonAssociatedDayOf)

      // nonAssociatedDayOf.forEach(removeRowFromDayOf)
      function removeRowFromDayOf(dayOfStudyProps) {
        let range = dayOfStudyProps.getRangeByColHeader("Legal Name")
        let rowIdx = range.getRow()
        let sheet = range.getSheet();
        sheet.deleteRow(rowIdx)
      }
      

    }
    /*
     * @param curr: Day-Of Study
     * @param next: From Schedule
     */
    function updateCurrentRows(curr, next, eventRange) {
      
      next.forEach((props, idx) => {
        let headers = Object.keys(props);
        const dayOfStudyHeaders = Object.keys(curr[idx])
        const allHeaderList = [...Object.keys(curr[idx]), ...headers].filter(onlyUnique)

        allHeaderList.forEach(updateCurrentCell)

        function updateCurrentCell(key) {
          let currentValue = curr[idx][key]
          let otherNext = curr.find(currentDayOfStudy => ["Legal Name", "Email", "Phone Number"]
            .some(headerAsStr => curr[headerAsStr] === "" || currentDayOfStudy[headerAsStr] == "" ? false : props[headerAsStr] === currentDayOfStudy[headerAsStr]))
          let otherNextValue = !!otherNext ? otherNext[key] : null
          let nextValue = props.hasOwnProperty(key) ? props[key] : otherNextValue
          let isNextValueBlank = nextValue === "" 
          let isDates = isDateObjects(nextValue, currentValue)
          if (isNextValueBlank && isDates && !isDateSame(nextValue, currentValue)) {
            updateCurrentCellRange(key, props, nextValue) //props.getRangeByColHeader(header).setValue(nextValue)
          } else if (isNextValueBlank && !isDates && nextValue !== currentValue) {
            updateCurrentCellRange(key, curr[idx], nextValue)
          }
        }
      })

      function updateCurrentCellRange(header, props, nextValue) {
        if (!props.hasOwnProperty('getRangeByColHeader')) {
          debugger
        }
        debugger
        if (props.getRangeByColHeader("Notes field").getValue() === 'test note field'){
          debugger
        }
        let updateRange = props.getRangeByColHeader(header)
        let rule = updateRange.getDataValidation();
        if (rule != null) {
          let acceptedValues = rule.getCriteriaValues().flat();
          let isValidDataValidation = acceptedValues.includes(nextValue)
          let criteria = rule.getCriteriaType();
          let currValue = updateRange.getValue();
          if (currValue == 'test note field'){
            debugger
          }
          if (isValidDataValidation && currValue !== "") {
            Logger.log(`nextValue: ${JSON.stringify(nextValue)}`)
            updateRange.setValue(nextValue)
          } else {
            return
          }
          Logger.log('The data validation rule is %s %s', criteria, acceptedValues);
        } else {

          updateRange.setValue(nextValue)
        }
      }
    }

  }
  function getDayOfStudyCurrentSessions(activeSpreadsheet) {
    try {
      let [...args] = [DATA.SheetNames.DayOfStudy.name, DATA.SheetNames.DayOfStudy.headerStartIdx, DATA.SheetNames.DayOfStudy.dataStartIdx]
      const sheet = activeSpreadsheet.getSheetByName(DATA.SheetNames.DayOfStudy.name)
      const { body: sessions } = getDataBySheet(sheet, DATA.SheetNames.DayOfStudy.headerStartIdx, DATA.SheetNames.DayOfStudy.dataStartIdx)
      return sessions
    } catch (e) {
      return null
    }
  }
  function isSameUXtendData(curr, next) {
    return curr.length === next.length ? curr.every(compareUxtendWith) : false

    function compareUxtendWith(props, idx) {
      let nextRow = next[idx]
      // let currKeys = Object.keys(props)
      // let nextKeys = Object.keys(nextRow)
      for (let header in props) {
        let currValue = props[header]
        let nextValue = nextRow[header]

        // let nv2 = nextRow["Time(PST)"]
        let isDateObj = (prop) => prop instanceof Date
        let isDateSame = (date1, date2) => date1.getTime() == date2.getTime()
        let isDateObjects = (...args) => args.every(isDateObj)
        let isSame = isDateObjects(currValue, nextValue) ? isDateSame(props[header], nextRow[header]) : JSON.stringify(currValue) === JSON.stringify(nextValue)
        if (!isSame && nextValue !== undefined) {
          // let three = JSON.stringify(currValue) === JSON.stringify(nextValue)
          let nextHeader = Object.keys(nextRow)[idx]
          // let sameheader = nextHeader == header
          return false
        }
      }
      return true
    }

  }
}

function createUXtendTab(activeSpreadsheet, _Browser) {
  activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.openById("1JTc66QWSJ5Fhf7MzLPet2fy1SMd0uTsmKraLWHruTsk")
  // ss.setActiveSheet
  _Browser = _Browser || { msgBox: (test) => Logger.log(test) }
  // Logger.log(activeSpreadsheet.getId())
  const uxTendTab = _createUXtendTab(activeSpreadsheet, _Browser)
  return uxTendTab

  function _createUXtendTab(activeSpreadsheet, _Browser) {
    const hasSheetNameBySpreadsheet = (ss) => (sheetName) => ss.getSheetByName(sheetName)
    activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.getActiveSpreadsheet()
    removeCurrentUxtendSheet(activeSpreadsheet)
    const uxTendSheetTemplate = DATA.SheetNames.UXtend.Template
    const { createUxtendTemplate } = fetchUxtendTemplate(uxTendSheetTemplate, activeSpreadsheet);
    const sessionData = fetchSessionsFromScheduleTab({ scheduleTab: fetchScheduleTab(activeSpreadsheet) });
    // short-circuit out if not session data found
    if (sessionData.length === 0) {
      throw (`Unable to find a participants within "Schedule" sheet to create "Day-Of Study" sheet`)
      return
    }
    const updateUxtendTab = addSessionDataToUXtendSheet({ uxTendTab: createUxtendTemplate(), sessionData })
    let timeRange = updateUxtendTab.getRange(DATA.SheetNames.UXtend.TimeRange)
    timeRange.setNumberFormat(DATA.SheetNames.UXtend.TimeNumberFormat);
    let pptTmzRange = updateUxtendTab.getRange(DATA.SheetNames.UXtend.PPTTMZ);
    let pptTmzFormulas = new Array(20).fill(null).map((_, idx) => [`=IFERROR(PPTTMZ(A${4 + idx},B${4 + idx},$B$2,D${4 + idx}), "")`])
    pptTmzRange.setFormulas(pptTmzFormulas)
    // ppttmz.set
    /*
     // let ppttmz = updateUXtendTab.getRange(DATA.SheetNames.UXtend.PPTTMZ);
    // debugger
    // ppttmz.setFormulas(ppttmzFormulas)
    */
    SpreadsheetApp.flush()
    return updateUxtendTab
  }
  function fetchUxtendTemplate({ templateId, sheetName }, activeSpreadsheet) {
    let templateSheet = SpreadsheetApp.openById(templateId).getSheetByName(sheetName)
    Logger.log('fetching uxtend create, fetch')
    return {
      createUxtendTemplate,
      fetchUxtendTab
    }


    function fetchUxtendTab() {
      return ss.getSheetByName(DATA.SheetNames.DayOfStudy.name)
    }
    function createUxtendTemplate() {
      Logger.log('createuxtendtemplate')
      let dosSheet = activeSpreadsheet.getSheetByName(DATA.SheetNames.DayOfStudy.name)
      !!dosSheet && activeSpreadsheet.deleteSheet(dosSheet)
      let newSheet = templateSheet.copyTo(activeSpreadsheet)
      newSheet.setName(DATA.SheetNames.DayOfStudy.name);
      Logger.log(`row: ${DATA.SheetNames.DayOfStudy.rangeIdxDataStart}, ${1}, ${newSheet.getLastRow() - DATA.SheetNames.DayOfStudy.rangeIdxDataStart + 1}, ${newSheet.getLastColumn()}`)
      let contentRange = newSheet.getRange(DATA.SheetNames.DayOfStudy.rangeIdxDataStart, 1, newSheet.getLastRow() - DATA.SheetNames.DayOfStudy.rangeIdxDataStart + 1, newSheet.getLastColumn())
      contentRange.clearContent();
      return newSheet
    }

  }


  // used on create "Day-Of Study" Sheet appends session data
  function addSessionDataToUXtendSheet({ uxTendTab, sessionData }) {
    const uxTendSheetHeadersAsObj = getHeadersAsObj({ sheet: uxTendTab, headerIdx: 3 })
    const uxTendRowsAsProps = sessionData.map(sessionToDayOf(uxTendSheetHeadersAsObj));
    const appendRowToSheet = (values, idx, arr) => applyBackGroundColor(uxTendTab.appendRow(values), idx, arr);

    const uxTendValues = uxTendRowsAsProps.reduce((acc, props) => {
      let headers = Object.keys(uxTendSheetHeadersAsObj);
      let values = []
      headers.forEach((str, idx) => props.hasOwnProperty(str) ? values[idx] = props[str] : null)
      return acc.concat([values])
    }, [])

    uxTendValues.forEach(appendRowToSheet)
    return uxTendTab
  }

}
function applyBackGroundColor(sheet, idx, arr) {
  let whiteBackground = "#ffffff"
  let greyBackground = "#e8eaed"
  let alternateColor = (color) => color === greyBackground ? whiteBackground : greyBackground
  let prevBackGroundColor = sheet.getRange(idx + 3, 1).getBackground()
  let prevRow = arr[idx - 1]
  let previousDate = prevRow === undefined ? null : prevRow[0]
  let currentDate = arr[idx][0]
  let isSameAsPrevDate = hasSameDate(previousDate, currentDate, idx)
  let backgroundColor = isSameAsPrevDate ? prevBackGroundColor : alternateColor(prevBackGroundColor)
  let rowRange = sheet.getRange(idx + 4, 1, 1, sheet.getLastColumn())
  rowRange.setBackground(backgroundColor)


  function hasSameDate(date1, date2, idx) {
    try {
      if (idx === 0) {
        return true
      }
      return ['getMonth', 'getFullYear', 'getDate'].every(fnAsStr => date1[fnAsStr]() === date2[fnAsStr]())
    } catch (e) {
      return false
    }
  }
}

function fetchSessionsFromScheduleTab({ scheduleTab }) {
  const values = scheduleTab.getDataRange().getValues();
  const headerRowName = DATA.SheetNames.Schedule.SessionHeader
  const sessionDetailIndex = findCoordByHeaderName(headerRowName)(values)
  const sessionDetails = getSessionDetails(scheduleTab, sessionDetailIndex)
  let temp = sessionDetails
  return sessionDetails

  function findCoordByHeaderName(headerName) {
    return function (values) {
      return values.reduce((acc, next, rowIdx) => {
        return !!acc ? acc : getIndicies(next, rowIdx)

        function getIndicies(next, rowIdx) {
          const hasColIdx = next.includes(headerName) ? next.indexOf(headerName) + 1 : null
          return !!hasColIdx ? { rowIdx: rowIdx + 2, colIdx: hasColIdx, headerName } : null
        }
      }, null)
    }
  }
  function getSessionDetails(sheet, { rowIdx, colIdx, headerName }) {
    const { body, headerObj } = getDataBySheet(sheet, rowIdx, rowIdx + 1);
    const sessionDetailRange = sheet.getRange(rowIdx, colIdx, sheet.getLastRow() - rowIdx, 1)
    const getCurrentRowIdx = (idx) => sheet.getRange(rowIdx + idx, colIdx).getRow() - 1
    const sessionValues = sessionDetailRange.getValues();

    const dateIndices = sessionValues.reduce((acc, arr, rowIdx) => {
      const isValidDate = (prop) => (new Date(prop)).toString() !== "Invalid Date"
      return isValidDate(arr[0]) ? acc.concat({ date: arr[0], rowIdx: getCurrentRowIdx(rowIdx) }) : acc
    }, []).sort((a, b) => a.rowIdx < b.rowIdx ? 1 : -1)
    const fetchSessionTimeDay = (props, idx, arr) => {
      let getDefaultSessionTime = (props) => {
        let headers = Object.keys(props)
        let header = headers[0]
        let rowIdxByHeader = props.getRangeByColHeader(header).getRow()
        return sheet.getRange(rowIdxByHeader, colIdx).getValue();
      }
      let sessionTimeKey = Object.keys(props).find(str => new Date(str).toString() !== "Invalid Date") || getDefaultSessionTime(props);
      let sessionTime = props.hasOwnProperty(sessionTimeKey) ? props[sessionTimeKey] : getDefaultSessionTime(props);

      Logger.log(`sessiont Time : ${sessionTime}`)
      try {
        sessionTime = sessionTime.replace(/\s+\-.*/gi, "");
      } catch (e) {
        throw (`time format of ${props["Email"]} is Time is Invalid`)
      }
      let rowRange = props.getRangeByColHeader(DATA.SheetNames.Schedule.FilterHeader)
      let sessionRowIdx = rowRange.getRow()
      try {
        let { date: sessionDay } = dateIndices.find(({ rowIdx }) => (rowIdx < sessionRowIdx))
        return Object.assign(props, { sessionTime, sessionDay })
      } catch (e) {
        return props
        throw (`Unable to read Session Date format${e}`)
      }
    }

    const condtionalValidScheduleRow = props => props[DATA.SheetNames.Schedule.FilterHeader] !== "";
    let sessions = body.filter(condtionalValidScheduleRow)

    Logger.log(JSON.stringify(sessions[1]));
    let sessionDetails = sessions.filter(condtionalValidScheduleRow).map(fetchSessionTimeDay)
    return sessionDetails
  }
}
function fetchScheduleTab(activeSpreadsheet) {
  try {
    return activeSpreadsheet.getSheetByName(DATA.SheetNames.Schedule.name)
  } catch (e) {
    throw (`unable to fetch "${DATA.SheetNames.Schedule.name}" Sheet name: ${e}`)
  }
}


const SESSIONtoDAYOFMap = {
  "Day": "sessionDay",
  "Time": "sessionTime",
  "Participant Timezone\n(Should pull from Schedule tab)": "Timezone",
  "Legal Name": "Legal Name",
  "Email": "Email",
  "Phone Number": "Phone Number",

}

function sessionToDayOf(dayOfStudyHeaderObj) {
  return function (sessionProp) {
    const headerCol = dayOfStudyHeaderObj
    const sessionKey = SESSIONtoDAYOFMap[headerCol];
    const fetchDayOfStudyValue = (props, sessionKey) => (headerStr) => props.hasOwnProperty(headerStr) ? props[headerStr] : SESSIONtoDAYOFMap.hasOwnProperty(headerStr) ? props[SESSIONtoDAYOFMap[headerStr]] : null
    return Object.keys(dayOfStudyHeaderObj).reduce((acc, next, idx, arr) => {
      let value = fetchDayOfStudyValue(sessionProp, SESSIONtoDAYOFMap)(next)
      return value === null ? acc : Object.assign(acc, { [next]: value })
    }, {})
  }
}


function removeCurrentUxtendSheet(activeSpreadsheet) {
  if (activeSpreadsheet.getSheetByName(DATA.SheetNames.DayOfStudy.name)) {
    try {
      activeSpreadsheet.deleteSheet(activeSpreadsheet.getSheetByName(DATA.SheetNames.DayOfStudy.name))
    } catch (e) { }
  }
}

