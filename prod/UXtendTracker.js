/** START Library end points */
function claimUnallocatedStudies(activeSpreadsheet) {
  //dev env
  // activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.openById("18n2qIV0vPW7d6lDPy_YgZSueNHlDlfay5VuVREGHHr0");
  // activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.openById("1ZonWfOeaRehTzuLx_Qnr1orFA0u_eDOk1liddwFny2w");
  const { claimUnallocatedStudies } = StudyTrackerLibrary(activeSpreadsheet)
  //  claimUnallocatedStudies()
  //  debugger
  return claimUnallocatedStudies()
}
function closeActiveStudies(activeSpreadsheet) {
  //dev env
  // activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.openById("1CMb5roBYnGEMajNS0B8Cer6s0m0HoRLqxX-XdgjrrCI");
  const { closeActiveStudy } = StudyTrackerLibrary(activeSpreadsheet)
  return closeActiveStudy()
}
function activeStudiesNotification(activeSpreadsheet) {
  const { activeStudiesNotification } = StudyTrackerLibrary(activeSpreadsheet)
  return activeStudiesNotification()
}
function generateWeeklyHeatMap(activeSpreadsheet) {
  // activeSpreadsheet = activeSpreadsheet || SpreadsheetApp.openById("11ug8uFFr_jHBKcK3MYlR1sN-77usbMBqtvJ5kA0-ZZE");
  const { generateWeeklyHeatMap } = StudyTrackerLibrary()
  return generateWeeklyHeatMap(activeSpreadsheet)
}
/** END Library end points */

function checkStudyTrackerVersion() {
  return "Production 1.9.0"
}

/** Some constants
 * general variables thats get used through the below code
 */
const TRACKERDATA = {
  SheetNames: {
    UnallocatedStudies: {
      Name: "UXtend Unallocated Studies",
      headerIdx: 3,
      bodyIdx: 4
    },
    ActiveStudies: {
      Name: "UXtend Active Studies",
      headerIdx: 3
    },
    ClosedStudies: {
      Name: "UXtend Closed Studies",
      headerIdx: 3,
      AttendenceStatus: {
        "Attended": "Error: Script Access to url or 'Day-Of Study' Not Found",
        "Cancelled": "Error: Script Access to urlor 'Day-Of Study' Not Found",
        "Cancelled by UXR": "Error: Script Access to url or 'Day-Of Study' Not Found",
      },
      ParticipantType: {
        "Scheduled": "Error: Script Access to url or 'Day-Of Study' Not Found",
        "Rescheduled": "Error: Script Access to url or 'Day-Of Study' Not Found",
        "Backup": "Error: Script Access to url or 'Day-Of Study' Not Found",
      },
    },
    DAYOFSTUDY: {
      Name: "Day-Of Study"
    }
  },
  //Spreadsheet external
  SS: {
    StaffingTrix: {
      id: "1ZoGLcVi_s7rZQgUx-Gan8rwK89yTTQsbuJm1BqTF1Vc",
      Personnel: "Personnel",
      COLHEAD: {
        PODLEAD: "POD LEAD",
        ldap: "ldap",
        STATUS: "In/Active"

      }
    },
    GOSTUDYQUEUE: {
      id: "",
      Sheets: {
        ACTIVE: {
          name: "Active Studies",
          headerIdx: 0,
          bodyIdxStart: 1
        }
      }
    },
    HTMLTEMPLATE: {
      Unallocated: { id: '198M3_9aGeHs46uFr2DygwolUWxKB3DRgvcP7nSEufy8' },
      // Claimed: {id: ''}
    }
  },
  Notification: {
    recipient: "uxtend-leaders@google.com",
    UXtendMemberColHeader: "UXtend member",
    RC: "RC",
    UXtendMemberColIdx: 1 // base 1
  }
}
/** END constants */

function StudyTrackerLibrary(activeSpreadsheet) {
  // const newStudies = fetchNewUxtendStudies()
  // const {activeStudiesNotification} = updateTrackerSheet
  const compose = (...fns) => x => fns.reduceRight((y, f) => f(y), x);
  // const {ldap} = getSessionInfo()
  const getValidationResults = (validationList) => validationList.reduce((acc, fn) => fn(range) ? acc : acc += ` ${fn.name}: ${fn(range)}`, "");
  const isSelectSingleRow = ({ getNumRows }) => 1 === getNumRows()
  const isSelectSheetByText = (text) => ({ getSheet }) => getSheet().getName() === text;
  const isSelectActiveStudies = isSelectSheetByText(TRACKERDATA.SheetNames.ActiveStudies.Name);
  const getUXtendValueByRange = (range, rowIdx, colIdx) => range.getSheet().getRange(rowIdx, colIdx).getValue()
  const hasUXtendMember = (range) => range.hasOwnProperty('getRow') ? getUXtendValueByRange(range, range.getRow(), TRACKERDATA.Notification.UXtendMemberColIdx) !== "" : false
  const isNotTopThreeRow = (range) => range.hasOwnProperty('getRow') ? range.getRow() > 3 : false
  const isSelectUnallocatedSheet = ({ getSheet }) => getSheet().getName() === TRACKERDATA.SheetNames.UnallocatedStudies.Name;
  const hasStudyId = ({ getRow, getSheet }) => getSheet().getRange(getRow(), 1).getValue()
  const getIsValidSelection = (args) => (range) => {
    return args.every(fn => fn(range))
  }
  // const range = activeSpreadsheet.getActiveRange()

  try {
    return { claimUnallocatedStudies, closeActiveStudy, activeStudiesNotification, generateWeeklyHeatMap }

  } catch (e) {
    Logger.log(`errors: ${e}`)
  }

  function generateWeeklyHeatMap(activeSpreadsheet, _Browser) {
    /** constanst **/
    const HEATMAP = {
      Sheets: {
        DAYOFSTUDY: {
          name: "Day-Of Study",
          headerStartIdx: 2,
          dataStartIdx: 3,
          Criteria: ["Legal Name", 'Email'],
          daysOut14: () => true
        },
        WeeklyHeatMap: {
          name: "Weekly Heatmap"
        }

      },
      defaultTextStyle: SpreadsheetApp.newTextStyle()
        .setFontSize(10)
        .setBold(false)
        .build()
    }
    const getMMddYYY = (props) => {
      let date = new Date(props);
      let isValidDate = date.toString() !== "Invalid Date"
      try {
        return isValidDate ? `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}` : "Invalid Date";
      } catch (e) {
        Logger.log(JSON.stringify(props))
        Logger.log(`convertion date error: ${e}`)
        return `Invalid Date ${JSON.stringify(props)}`
      }
    }

    return _generateWeeklyHeatmap(activeSpreadsheet, _Browser)

    /** main function **/
    function _generateWeeklyHeatmap(activeSpreadsheet, _Browser) {
      // const dateTimeFilter = (props)
      const active = getActiveStudies({ spreadsheet: activeSpreadsheet }, undefined);
      const closed = getClosedStudies({ spreadsheet: activeSpreadsheet }, undefined)

      const activeStudies = active.map(getSpreadsheetProps)
        // .filter(props => props !== null)
        // .filter(props => !!props.dayOfStudy)
        .map(getDayOfStudyCurrentSessions);
      Logger.log(`activeStudies Num: ${activeStudies.length}`)
      // const unableToFetchStudies = active.filter(props => getSpreadsheetProps(props) === null)
      const closedStudies = closed.map(getSpreadsheetProps)
        // .filter(props => props !== null)
        // .filter(props => !!props.dayOfStudy)
        .map(getDayOfStudyCurrentSessions);
      Logger.log(`closedStudies Num: ${activeStudies.length}`)

      const getStudyProps = ({ UXtendMember, url, sessions, tmz, ssTmz, studyName }, idx) => {

        return sessions.map(function (session) {
          let sessionTimeCol = Object.keys(session).find(str => str.toLowerCase().includes("time")) || "Time"
          let temp = session["Time"]
          /*
          let timeColumn = Object.keys(session).findIndex(str => str.toLowerCase().includes("time"))
                   let hasSessionTimeCol = timeColumn !== -1
                   let sessionTimeCol = hasSessionTimeCol || 1
                   */
          // const isDate = session[sessionTimeCol]

          return {
            UXtendMember,
            url,
            date: getMMddYYY(session.Day),
            time: session[sessionTimeCol],
            participantType: session["Participant Type"] !== "" ? session["Participant Type"] : "Pending...",
            tmz,
            ssTmz,
            studyName
          }
        })
      }

      const getFirstTimeMatch = (str) => {
        try {
          if (str.trim() === "") {
            return "Time Entry Blank"
          }
          let firstTimeFound = str.match(/(\d+:\d+\s?\w+)/gi)
          return firstTimeFound ? firstTimeFound[0] : str
        } catch (e) { return str }
      }
      // below #2 used for testing
      //       const groupStudyTemplateData2 = activeStudies.flatMap(getStudyProps)
      // .map(convertDateTimeToPT)
      // .filter(timeDateRange)
      // .reduce(groupStudiesByDate, [])
      const activeAndClosedStudies = [...activeStudies, ...closedStudies];
      const hasDayOfStudy = (props) => {
        if(props === null){
          return false
        }
        return props.hasOwnProperty('dayOfStudy') && (props.dayOfStudy !== undefined)
      }

      const a = activeAndClosedStudies
        .filter(hasDayOfStudy) 
        debugger

      const groupStudyTemplateData = activeAndClosedStudies
        .filter(hasDayOfStudy) //filters for only study with "Day-Of Study" Sheet
        .flatMap(getStudyProps)
        .map(convertDateTimeToPT)
        .reduce(groupStudiesByDate, [])
        .sort(sortByStudyDate)
        .map(studyTimeSorter)
        .map(props => Object.assign(props, { byStudy: props.studyData.reduce(groupStudiesTrix, []).sort(sortByUXtendMember), }))
        .filter(timeDateRange)
      debugger
      // let apple = group
      const heatMapData = getSummary(groupStudyTemplateData);
      Logger.log(`updating heat map fn`)
      updateWeeklyHeatMap(heatMapData)

      SpreadsheetApp.flush()

      return {
        status: `Heatmap updated as of ${new Date}`
      }

      /*
      * Description: converts date/time tmz from trix to PT 
      * @params Object (date: String)
      * @params Object (time: String)
      * @params Object (tmz: String)
      * @return Object (...props, Object(time:String, date: String))
      */
      function convertDateTimeToPT({ date, time, tmz, ...props }, idx) {
        const converTime = _convertDateTimeToPT({ time, date, tmz, })
        return Object.assign(props, converTime)

        // return Object.assign(props, _convertDateTimeToPT({ time, date, tmz, ...args }))

        function _convertDateTimeToPT({ time, date, tmz }) {

          try {
            let regexTime = getFirstTimeMatch(time.toString())
            let getGMTofTMZ = (tmzAsStr) => {
              let tmzRegex = /GMT(-|\+)\d+:\d+/gi.exec(tmzAsStr)
              return tmzRegex === null ? tmzAsStr : tmzRegex[0]
            }
            let gmtOfTmz = getGMTofTMZ(tmz)
            let dateObjWithTmz = new Date(`${date} ${regexTime} ${gmtOfTmz}`);
            // tmz: "GMT-8:00" is hardcoded b/c variable tmz is pulled infrom ss where its only has standard time, and therefore on the heatmap to repersent PST daylight saving isn't accounted for
            let isValidDate = dateObjWithTmz.toString() !== "Invalid Date"
            let dateWithTmzAsStr = isValidDate ? Utilities.formatDate(dateObjWithTmz, "GMT-8:00", "MM/dd/yyyy_h:mm a") : [date, time].join("_")
            let [dateConvert, timeConvert] = dateWithTmzAsStr.split('_');
            return {
              date: dateConvert,
              time: timeConvert
            }
          } catch (e) {
            Logger.log(`convert time error: ${time}, ${date}`)
            return { date, time }
          }
        }
      }

      function timeDateRange({ date: trixDate, ...props }) {
        const daysOut = new Date()
        daysOut.setDate(daysOut.getDate() + 30)
        const today = new Date()
        today.setDate(today.getDate() - 10)
        trixDate = new Date(trixDate)
        const isWith14Days = today < trixDate && trixDate < daysOut

        return isWith14Days
      }

      function getSummary(arr) {
        const organizeStudyByMember = ({ UXtendMember, url, Studies, studyName }) => Studies.map(props => Object.assign({}, { ...props, UXtendMember, url, studyName }))
        const organizeByDate = (organizeStudyByMember) => (props) => Object.assign({}, { date: props.date, studies: props.byStudy.map(organizeStudyByMember), })
        const organizeStudiesByDateByMember = organizeByDate(organizeStudyByMember)
        return {
          dateCount: arr.length,
          dates: arr.map(organizeStudiesByDateByMember),
        }
      }
      function updateWeeklyHeatMap(heatMapData) {
        /** Sheet data here */
        const sheet = activeSpreadsheet.getSheetByName(HEATMAP.Sheets.WeeklyHeatMap.name)
        const existingRangeList = sheet.getRangeList(["B3:6", "B7:102"]);
        const totalsRange = "b103:104"
        existingRangeList.clearContent()
          .setBackground("#ffffff")
          .breakApart();
        sheet.getRange(totalsRange)
          .clearContent()
          .breakApart()

        const getRangeByCoord = ({ columnStart = 1, rowStart = 1, rowEnd = 1, columnEnd = 1 }) => sheet.getRange(rowStart, columnStart, rowEnd, columnEnd);
        const { dates: heatMapDates, dateCount } = heatMapData
        const breakApartRanges = ({ range, ...props }) => {
          let mergedRanges = range.getMergedRanges()
          mergedRanges.forEach(range => range.breakApart())
          return { range, ...props }
        }
        const setupSpacerRange = (range) => {
          breakApartRanges({ range })
          range = range.setBackground('#e8eaed').clearContent().merge()
          sheet.setColumnWidth(range.getColumn(), 20)
          return range
        }
        heatMapDates.forEach(updateHeatMapSS)

        function updateHeatMapSS({ date, studies, }, heatMapDateIdx, arr) {

          let numOfStudies = studies.length
          let previousNumOfStudies = heatMapDateIdx !== 0 ? arr.slice(0, heatMapDateIdx).reduce((acc, next) => acc + next.studies.length, 0) : heatMapDateIdx;

          const backgroundColor = (heatMapDateIdx + 1) % 2 == 0 ? "#ffffff" : "#e4f1f9"

          setDayHoursOfDayValue();
          setStudyTabUXtendMember(studies)

          function setStudyTabUXtendMember(studies) {

            const studiesProps = fetchStudiesURLandMember(studies)
            const setRangeProps = ({ range, numberFormat, value }) => (!numberFormat)
              ? setRangePropertiesWithOutNumformat({ range, numberFormat, value })
              : setRangePropertiesWithNumformat({ range, numberFormat, value })

            const setValueAndFormat = compose(applyUpdateViaCallback, breakApartRanges)

            studiesProps.forEach(setStudyTabAndUXtendMemberValues)

            function setStudyTabAndUXtendMemberValues({ UXtendMember, url, studiesByMember, studyName }, studyIdx) {
              const heatMapColIdx = previousNumOfStudies + 2 + heatMapDateIdx + studyIdx
              const studyColumnStart = previousNumOfStudies + 2 + heatMapDateIdx + studyIdx
              const setPartipantType = setPartipantTypeByDayOfStudy(sheet)
              //Loops through Studies and sets "Study Tab" and "UXtend Member" cells
              _setStudyTabAndUXtendMemberValues(studyName)
              //Loops through studies by member and sets participant type
              debugger
              studiesByMember.map(setParticipationByStudy);
              setTotalsPerStudy(studiesByMember.length)

              function setParticipationByStudy({ time, participantType }, studyByMemberIdx, studyMembersArr) {
                return setPartipantType({ time, colIdx: heatMapColIdx, participantType })
              }
              function _setStudyTabAndUXtendMemberValues(studyName) {
                /** have hours or day be individual over merge */
                const rangeHoursOfDay = sheet.getRange(4, studyColumnStart);
                const hoursOfDayPTRowObj = { range: rangeHoursOfDay, numberFormat: "mm/dd/yyyy", backgroundColor, cb: setRangePropertiesWithNumformat, value: date }
                let rangeStudyTab = getRangeByCoord({ rowStart: 5, columnStart: studyColumnStart });
                let rangeUXtendMember = sheet.getRange(6, studyColumnStart);
                let studySheetValue
                try {
                  let { getName: getTrixName, getUrl: getTrixUrl } = SpreadsheetApp.openByUrl(url)
                  studySheetValue = SpreadsheetApp.newRichTextValue()
                    .setText(studyName)
                    .setLinkUrl(getTrixUrl())
                    .build()
                } catch (e) {
                  studySheetValue = url
                }
                let studyTabRowObj = { range: rangeStudyTab, numberFormat: null, value: studySheetValue, cb: setRangeRichTextProps }
                let UXtendMemberRowObj = { range: rangeUXtendMember, numberFormat: null, value: UXtendMember, cb: setRangeProps }
                /** add hoursOfDay to below so its updates as uxtendmembers are*/
                let updateRanges = [studyTabRowObj, UXtendMemberRowObj, hoursOfDayPTRowObj]
                updateRanges.forEach(setValueAndFormat)
              }

              function setTotalsPerStudy(count) {
                /** totals per study */
                const offPurple = "#d9d2e9"
                let rangeTotalsPerStudy = getRangeByCoord({ rowStart: 103, rowEnd: 1, columnStart: studyColumnStart, backgroundColor: offPurple })
                const totalsPerStudyRowObj = { range: rangeTotalsPerStudy, backgroundColor: offPurple, cb: setRangePropertiesWithOutNumformat, value: count }
                const setValueAndFormat = compose(applyUpdateViaCallback, breakApartRanges);
                setValueAndFormat(totalsPerStudyRowObj)
              }
            }
          }

          function setDayHoursOfDayValue() {
            const columnStart = previousNumOfStudies + 2 + heatMapDateIdx
            const columnEnd = numOfStudies
            let rangeDay = getRangeByCoord({ rowStart: 3, columnStart, columnEnd })
            let rangeHoursOfDayPT = getRangeByCoord({ rowStart: 4, columnStart, columnEnd })
            let headerRangeSpacer = getRangeByCoord({ rowStart: 3, rowEnd: 4, columnStart: columnEnd + columnStart })
            let bodyRangeSpacer = getRangeByCoord({ rowStart: 7, rowEnd: 96, columnStart: columnEnd + columnStart })
            let columnSpacers = [headerRangeSpacer, bodyRangeSpacer]
            const setValueAndFormat = compose(applyUpdateViaCallback, breakApartRanges);
            /**  */
            // const backgroundColor = (heatMapDateIdx + 1) % 2 == 0 ? "#ffffff" : "#e4f1f9"
            const dayRowObj = { range: rangeDay, numberFormat: "dddd", backgroundColor, cb: setRangePropertiesWithNumformat, value: date }
            const hoursOfDayPTRowObj = { range: rangeHoursOfDayPT, numberFormat: "mm/dd/yyyy", backgroundColor, cb: setRangePropertiesWithNumformat, value: date }
            /** totals per day */
            const offYellow = "#fff2cc"
            let rangeTotalsPerDay = getRangeByCoord({ rowStart: 104, rowEnd: 1, columnStart, columnEnd, backgroundColor: offYellow })
            const totalsPerDayRange = getRangeByCoord({ rowStart: 103, rowEnd: 1, columnStart, columnEnd })
            const totalsPerDayRowObj = { range: rangeTotalsPerDay, backgroundColor: offYellow, cb: setRangePropertiesWithformula, formula: `=SUM(${totalsPerDayRange.getA1Notation()})` }

            let updateRanges = [dayRowObj, totalsPerDayRowObj,]
            // let updateRanges = [dayRowObj, hoursOfDayPTRowObj, totalsPerDayRowObj,]
            setValueAndFormat(hoursOfDayPTRowObj)
            updateRanges.forEach(setValueAndFormat)
            columnSpacers.forEach(setupSpacerRange)
          }

        }
      }
      function groupStudiesTrix(acc, next) {
        let { UXtendMember, url, time, participantType, studyName } = next
        let findStudyByMemberAndUrl = acc.find(accProps => (accProps["UXtendMember"] === UXtendMember) && accProps["url"] === url);
        let timeParticipantProps = { time: next.time, participantType: next.participantType }

        return !!findStudyByMemberAndUrl
          ? appendStudy(acc, findStudyByMemberAndUrl, timeParticipantProps)
          : acc.concat({ UXtendMember, url, Studies: [timeParticipantProps], studyName })
      }
      function appendStudy(acc, props, timeParticipantProps) {
        props.Studies = props.Studies.concat(timeParticipantProps)
        return acc
      }

    }

    function trixWithinTimeDateRange({ "What is the first date of the study?": startDate, ...props }) {
      const daysOut = new Date()
      daysOut.setDate(daysOut.getDate() + 22)
      const today = new Date()
      today.setDate(today.getDate() - 10)
      const studyDate = new Date(startDate)

      const isWithinRange = today < studyDate //&& studyDate < daysOut
      return isWithinRange
    }


    /** generate heat map general functions **/

    function getActiveStudies({ spreadsheet, _Browser }) {
      try {
        const sheet = spreadsheet.getSheetByName("UXtend Active Studies");
        const { body } = getDataBySheet(sheet, 2, 3)
        return body.filter(trixWithinTimeDateRange)
          .map(({ "Study spreadsheet": url, "UXtend member": UXtendMember, "What's the name of the study?": studyName }) => Object.assign({}, { url, UXtendMember, studyName }))
      } catch (e) {
        return null
        throw (`Unable to find ${"UXtend Active Studies"} within ${spreadsheet.getName()}`)
      }
    }
    function getClosedStudies({ spreadsheet, _Browser }) {
      try {
        const sheet = spreadsheet.getSheetByName("UXtend Closed Studies");
        const { body } = getDataBySheet(sheet, 2, 3)
        return body.filter(trixWithinTimeDateRange)
          .map(({ "Study spreadsheet": url, "UXtend member": UXtendMember, "What's the name of the study?": studyName }) => Object.assign({}, { url, UXtendMember, studyName }))
      } catch (e) {
        return null
        throw (`Unable to find ${"UXtend Active Studies"} within ${spreadsheet.getName()}`)
      }
    }
    function setPartipantTypeByDayOfStudy(sheet) {
      const rangeTimez = sheet.getRange("A7:A102")
      const hoursList = rangeTimez.getValues().map(a => a[0])

      return setPartipantType

      function setPartipantType({ time, colIdx, participantType }) {
        const timeFound = hoursList.find(availableTime => time === availableTime)
        const timeRowIdx = hoursList.findIndex(availableTime => time === availableTime) + 7
        const cell = sheet.getRange(timeRowIdx, colIdx);
        const value = cell.getValue() === "" ? participantType : `${cell.getValue()}, ${participantType}`
        if (!timeFound) {
          let unmatchedRange = sheet.getRange(102, colIdx)
          let noMatchValue = JSON.stringify({ time, participantType })
          let cellUnmatchedValue = unmatchedRange.getValue() === "" ? noMatchValue : unmatchedRange.getValue().concat(`, ${noMatchValue}`)
          unmatchValue = unmatchedRange.setValue(cellUnmatchedValue).setWrap(true)
          Logger.log(`unable to find Time match ${noMatchValue} url: ${sheet.getParent().getUrl()}`)

        }
        return timeFound
          ? { range: cell.setValue(participantType), value, }
          : { range: null, value, }
      }
    }

    function setRangePropertiesWithNumformat({ range, numberFormat, value, backgroundColor = "#ffffff" }) {
      let a1 = range.getA1Notation()
      range.merge()
        .setValue(value)
        .setNumberFormat(numberFormat)
        .setHorizontalAlignment("Center")
        .setBackground(backgroundColor)
      range.getSheet().setColumnWidth(range.getColumn(), 100)
      range.setTextStyle(HEATMAP.defaultTextStyle)
      return range
    }
    function setRangePropertiesWithformula({ range, numberFormat, formula, backgroundColor = "#ffffff" }) {
      let a1 = range.getA1Notation()
      range.merge()
        .setHorizontalAlignment("Center")
        .setBackground(backgroundColor)
        .setFormula(formula)
      range.getSheet().setColumnWidth(range.getColumn(), 100)
      range.setTextStyle(HEATMAP.defaultTextStyle)
      return range
    }
    function setRangePropertiesWithOutNumformat({ range, numberFormat, value, backgroundColor = "#ffffff" }) {
      range.merge()
        .setValue(value)
        // .setNumberFormat(numberFormat)
        .setHorizontalAlignment("Center")
        .setBackground(backgroundColor)
      range.getSheet().setColumnWidth(range.getColumn(), 100)
      range.setTextStyle(HEATMAP.defaultTextStyle)
      return range
    }
    function setRangeRichTextProps({ range, numberFormat, value, backgroundColor = "#ffffff" }) {
      range.merge()
        .setRichTextValue(value)
        .setHorizontalAlignment("Left")
        .setBackground(backgroundColor)
      range.setTextStyle(HEATMAP.defaultTextStyle)
      range.getSheet().setColumnWidth(range.getColumn(), 100)
      return range
    }

    function fetchStudiesURLandMember(arr) {
      return arr.map(studyArr => {
        let { UXtendMember, url, studyName } = studyArr[0]
        let studiesByMember = studyArr.map(({ time, participantType }) => Object.assign({}, { time, participantType }))
        return { UXtendMember, url, studiesByMember, studyName }
      })
    }
    function applyUpdateViaCallback({ cb, ...props }) {
      cb(props)
    }



    function groupStudiesByDate(acc, dayOfStudy, idx, arr) {
      let { date, url, studyName } = dayOfStudy
      const hasDateAndUrl = acc.find(({ date: accDate, url: accUrl }) => {
        let isSameDate = getMMddYYY(date) === getMMddYYY(accDate)
        let isSameUrl = accUrl === url
        return (isSameDate && isSameUrl)
      });


      const hasDate = acc.find(({ date: accDate }) => getMMddYYY(date) === getMMddYYY(accDate));
      !!hasDate && Object.assign(hasDate, { studyData: [...hasDate.studyData.concat(dayOfStudy)] })
      return !!hasDate ? acc : acc.concat({ date: date, studyData: [dayOfStudy] }) //.sort(sortByStudyDate)
    }
    // function sortByStudyDate(a, b) {
    //   let aDate = new Date(a.date)
    //   let bDate = new Date(b.date)
    //   Logger.log
    //   return new Date(a.date) < new Date(b.date) ? -1 : 1
    // }
    function sortUXtendAscending(a, b) {
      debugger
    }
    function sortByStudyDate(a, b) {
      const key1 = new Date(a.date.toString());
      const key2 = new Date(b.date.toString());
      // Logger.log(` params a "${JSON.stringify(a.date)}" date b "${JSON.stringify(b.date)}"`);
      // Logger.log(` date key1 ${JSON.stringify(key1)} date key2 ${JSON.stringify(key2)}`);
      // Logger.log(`key1 < key2 ${key1 < key2}`);
      // Logger.log(`key1 === "Invalid Date" ${key1 === "Invalid Date"}`);
      // Logger.log(`key2 === "Invalid Date" ${key2 === "Invalid Date"}`);
      // if(){}
      if (key1 === "Invalid Date") {
        return 1
      }
      if (key2 === "Invalid Date") {
        return -1
      }

      // return key1 - key2
      let res = key1 < key2
      if (JSON.stringify(a.date) === '07/20/2021' || JSON.stringify(b.date) === '07/20/2021') {
        debugger
      }
      
      if (key1 < key2) {
        return -1;
      } else if (key1 == key2) {
        return 0;
      } else {
        return 1;
      }
      // function ascendUXtend(a,b){
      //   const key1 = a.
      // }
    }
    function sortByUXtendMember(a, b) {
      try {
        a = a.UXtendMember,
          b = b.UXtendMember
        if (a > b) return 1;
        if (a < b) return -1;
        return 0;
      } catch (e) {
        return
      }
    }
    function studyTimeSorter(props) {
      let { studyData } = props
      //sort time of studies within studyData
      studyData.sort(sortByTime)
      return props
    }

    function sortByTime({ time: timeA }, { time: timeB }) {
      let [oclockA, meridiemA] = timeA.split(/\s+/gi)
      let [oclockB, meridiemB] = timeB.split(/\s+/gi)
      const sortMeridiem = (a, b) => (a == b) ? null : (a === "AM") ? -1 : (b === "AM") ? 1 : 0
      const sortTime = (a, b) => {
        let timeAsNum = [a, b].map(str => Number(str.replace(/:.*/gi, "")))
        let timeSorted = timeAsNum.sort().reverse()
        return timeAsNum[0] === timeSorted[0] ? -1 : 1
      }
      let tm = sortMeridiem(meridiemA, meridiemB)
      let tts = sortTime(oclockA, oclockB)
      return tm === 0 ? tts : tm
    }


    function getSpreadsheetProps({ url, ...args }) {
      try {
        Utilities.sleep(200)
        const ss = SpreadsheetApp.openByUrl(url);
        const ssTmz = ss.getSpreadsheetTimeZone()
        const dayOfStudy = ss.getSheets().find(sheet => sheet.getName() === HEATMAP.Sheets.DAYOFSTUDY.name)
        return Object.assign({}, { ...args, url, dayOfStudy, ss, ssTmz })
      } catch (e) {
        Logger.log(`Caught Error with ss.openByUrl on ${url} \nerror: ${e} `)
        return null
      }
    }
    function getDayOfStudyCurrentSessions(props) {
      if (props === null){
        return null
      }
      try {
        const { ss, ...args } = props
        const sheet = ss.getSheetByName(HEATMAP.Sheets.DAYOFSTUDY.name);
        if (ss === null) {
          return null
        }
        if (sheet === null) {
          return Object.assign({}, { ...args, ss, sessions: [], tmz: null })
        }
        let tmz = sheet.getRange('B2').getValue()
        let { body: sessions } = getDataBySheet(sheet, HEATMAP.Sheets.DAYOFSTUDY.headerStartIdx, HEATMAP.Sheets.DAYOFSTUDY.dataStartIdx)
        sessions = sessions.filter(props => HEATMAP.Sheets.DAYOFSTUDY.Criteria.some(str => props[str] !== "")).filter(HEATMAP.Sheets.DAYOFSTUDY.daysOut14)
        Logger.log(`# of session's FOUND: ${sessions.length} url: ${ss.getUrl()}`)
        return Object.assign({}, { ...args, ss, sessions, tmz })
      } catch (e) {
        Logger.log(`no day of study found`)
        // return Object.assign({}, { ...args, ss, sessions: [], tmz: null })
        return null
      }
    }
  }

  function getSessionInfo() {
    const session = Session
    const user = session.getUser()
    const email = user.getEmail();
    const ldap = email.slice(0, email.indexOf('@'));
    return { session, user, email, ldap }
  }

  function closeActiveStudy() {
    let range = activeSpreadsheet.getActiveRange();
    const validationList = [isSelectSingleRow, isSelectActiveStudies, isNotTopThreeRow]
    let isValidSelection = getIsValidSelection(validationList)(range);
    let testv = validationList.map(fn => fn(range))
    let msg = `Check to Ensure the following:\\nOnly a single row selected\\nCurrently on on the ${TRACKERDATA.SheetNames.ActiveStudies.Name} Sheet`
    return isValidSelection ? appendRowValues({ ss: range.getSheet().getParent(), range, fromStartIdx: TRACKERDATA.SheetNames.ActiveStudies.headerIdx }, { toSheetName: TRACKERDATA.SheetNames.ClosedStudies.Name, toStartIdx: TRACKERDATA.SheetNames.ClosedStudies.headerIdx })
      : { currentSheet: activeSpreadsheet, nextSheet: null, status: false, msg }
  }


  function claimUnallocatedStudies() {

    let msg = ""
    const sheet = activeSpreadsheet.getSheetByName("UXtend Unallocated Studies")
    // let toActiveRange = sheet.getRange("q19")
    // let range = sheet.setActiveRange(toActiveRange) 
    // let rangea1 = range.getA1Notation();
    // let checkerval = [isSelectSingleRow, isSelectUnallocatedSheet, isNotTopThreeRow, hasUXtendMember].filter(fn => !fn(range))
    // // dev purpose
    // Logger.log(toActiveRange.getA1Notation())
    // const range = activeSpreadsheet.setActiveRange(toActiveRange)
    // const range = toActiveRange

    /** PRODUCTION */
    const range = activeSpreadsheet.getActiveRange();
    // Logger.log(range.getA1Notation())
    debugger
    const validationList = [isSelectSingleRow, isSelectUnallocatedSheet, isNotTopThreeRow, hasUXtendMember]
    let isValidSelection = getIsValidSelection(validationList)(range)
    const validationResults = validationList.reduce((acc, fn) => fn(range) ? acc : acc += ` ${fn.name}: ${fn(range)}`, "")
    const getUXtendMemberValue = (range) => range.hasOwnProperty('getRow') ? getUXtendValueByRange(range, range.getRow(), TRACKERDATA.Notification.UXtendMemberColIdx) : null
    const uxtendMemberValue = getUXtendMemberValue(range);
    const hasBlankUXtendValue = uxtendMemberValue === ""
    msg += `Check to Ensure the following:\\nOnly a single row selected\\nCurrently on on the ${TRACKERDATA.SheetNames.UnallocatedStudies.Name} Sheet`
    if (hasBlankUXtendValue) {
      msg += `UXtend Member value "${uxtendMemberValue}" may not be blank\\n`
    }
    let statusObj = isValidSelection ?
      appendRowValues({ ss: range.getSheet().getParent(), range, fromStartIdx: TRACKERDATA.SheetNames.UnallocatedStudies.headerIdx },
        { toSheetName: TRACKERDATA.SheetNames.ActiveStudies.Name, toStartIdx: TRACKERDATA.SheetNames.ActiveStudies.headerIdx })
      : { currentSheet: activeSpreadsheet, nextSheet: null, status: false, msg }

    return statusObj
  }
  function getRowAsObject({ range, headersIdx, isIndex }) {
    const sheet = range.getSheet();
    let rangeIdx = range.getRow();
    const [headers] = sheet.getRange(headersIdx, 1, 1, sheet.getLastColumn()).getValues();
    let rowIndices = sheet.getRange(range.getRow(), 1, 1, sheet.getLastColumn()).getValues()[0].map((header, idx) => idx)
    const [values] = isIndex
      ? rowIndices
      : sheet.getRange(range.getRow(), 1, 1, sheet.getLastColumn()).getValues()
    return headers.reduce((acc, header, idx) => {
      !!isIndex ? acc[header] = idx : acc[header] = values[idx]
      return acc
    }, {})
  }
  /* moves data from one sheet to another and removes from origin
   * @param { ss, range, fromStartIdx } from location info
   * @param { toSheetName, toStartIdx } to location info
   * @return undefined
   */
  function appendRowValues({ ss, range, fromStartIdx }, { toSheetName, toStartIdx }) {
    // removed 6/16/2021
    const setHeaderAsFilter = (sheet) => {
      try {
        let movedTimestampIdx = sheet.getRange(3, 1, 1, sheet.getLastColumn()).getValues()[0].indexOf("Moved Timestamp") + 1
        debugger
        let setFilter = (range) => range.createFilter();
        let removeFilter = (range) => { range.getFilter().remove(); return range }
        let sortFilter = (filterObj) => filterObj.sort(movedTimestampIdx, true);
        let lastRow = sheet.getLastRow();
        let lastColumn = sheet.getLastColumn();
        debugger
        let range = sheet.getRange(3, 1, sheet.getLastRow(), sheet.getLastColumn())
        let filter = range.getFilter()
        let updateFilter = compose(sortFilter, setFilter, removeFilter);
        let setNewFilter = compose(sortFilter, setFilter)
        !!filter ? updateFilter(range) : setNewFilter(range)

        // sheet.getRange(3, 1, sheet.getLastRow(), sheet.getLastColumn())
      } catch (e) {
        Logger.log(e)
      }
    }

    const nextSheet = range.getSheet().getParent().getSheetByName(toSheetName);
    const currSheet = range.getSheet();

    const nextRangeRow = nextSheet.getRange(3, 1, 1, nextSheet.getLastColumn())

    const curr = getRowAsObject({ range, headersIdx: fromStartIdx });
    const next = getRowAsObject({ range: nextRangeRow, headersIdx: toStartIdx, isIndex: true })
    curr["UXR location"] = curr.hasOwnProperty("Researcher Location") ? curr["Researcher Location"] : curr["UXR location"]
    curr["PL"] = curr["PL"] || "Pod Lead Not Found"
    curr["What is the first date of the study?"] = curr.hasOwnProperty("What is the targeted first date of the study?") ? curr["What is the targeted first date of the study?"] : curr["What is the first date of the study?"]
    curr["UXR"] = curr.hasOwnProperty("Email Address") ? curr["Email Address"] : curr["UXR"]
    curr["Product area"] = curr.hasOwnProperty("Product Area (recruiters do not edit this column)") ? curr["Product Area (recruiters do not edit this column)"] : curr["Product area"]
    curr["Study spreadsheet"] = curr.hasOwnProperty("Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uer-recruiters@google.com added as an editor/collaborator.") ? curr["Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uer-recruiters@google.com added as an editor/collaborator."] : curr["Study spreadsheet"]




    if (toSheetName === "UXtend Active Studies") {
      curr["Remote Tool"] = fetchRemoteToolForStudy(curr["Study spreadsheet"])
      curr["Timestamp Moved to Active"] = new Date();
      debugger
      try {
        sendActiveStudiesNotification(curr, toSheetName)
      } catch (e) {
        throw (`unable to send active studies email error: ${e}`)
      }


      function fetchRemoteToolForStudy(url) {
        const fetchSheet = (url) => compose(fetchDayOfStudySheet, fetchSS)(url)
        let dayOfStudySheet
        try {
          dayOfStudySheet = fetchSheet(url)
          let remoteToolForStudyRange = dayOfStudySheet.getRange("F2")
          return remoteToolForStudyRange.getValue()
        } catch (e) {
          return "Not Found"
        }
      }
      function sendActiveStudiesNotification(props) {
        let ldap = props.hasOwnProperty(TRACKERDATA.Notification.UXtendMemberColHeader) ? props[TRACKERDATA.Notification.UXtendMemberColHeader] : "";
        let rc = props.hasOwnProperty(TRACKERDATA.Notification.RC) ? props[TRACKERDATA.Notification.RC] : "";
        const addAtGoogle = (string) => typeof string === "string" ? `${string.trim()}@google.com` : ""
        let rcEmails = rc !== "" ? rc.split(',').map(addAtGoogle).join(", ") : ""
        const cc = rcEmails
        const emailAddress = `${ldap.trim()}@google.com`
        // const htmlBody = uxTendHtmlTemplate(props);

        const htmlBody = fetchClaimStudyHTMLTemplate(props, toSheetName);
        const subject = "UXtend Study Set to Active"
        debugger
        try {
          Logger.log(`cc: ${cc}`)
          debugger
          // GmailApp.sendEmail('douglascox@google.com', cc+subject, "UXtend Member Notification", { htmlBody, cc })
          GmailApp.sendEmail(emailAddress, subject, "UXtend Member Notification", { htmlBody, cc })
        } catch (e) {
          throw (`unable to send active studies email error: ${e}`)
        }
      }

    }
    if (toSheetName === "UXtend Closed Studies") {
      curr["Timestamp Moved to Closed"] = new Date()
    }
    let values = getNextRowAsValues(curr, next);
    if (toSheetName === "UXtend Closed Studies") {
      const studySpreadsheetUrl = curr["Study spreadsheet"];
      let dayOfStudyCounter = fetchDayOfStudyCounter(studySpreadsheetUrl);
      let nextRowHeaderValues = nextRangeRow.getValues()[0]
      for (let key in dayOfStudyCounter) {
        if (nextRowHeaderValues.includes(key)) {
          let colIdx = nextRowHeaderValues.indexOf(key)
          values[colIdx] = dayOfStudyCounter[key]

        }
      }
    }

    let updatedNextSheet = nextSheet.appendRow(values);

    let updatedCurrSheet = () => {
      try {
        currSheet.deleteRow(range.getRow());
        Logger.log(`removed row from UXtend Unallocated Studies`)
      } catch (e) {
        const rowRange = currSheet.getRange(range.getRow(), 1, 1, currSheet.getLastColumn())
        rowRange.clearContent()
      };
    }
    updatedCurrSheet()
    // setHeaderAsFilter(updatedNextSheet)

    return {
      nextSheet: updatedNextSheet,
      currentSheet: updatedCurrSheet,
      status: true,
      values
    }

    function getNextRowAsValues(curr, next) {
      let values = []
      for (let headerName in next) {
        let headerIdx = next[headerName]
        if (curr.hasOwnProperty(headerName) && headerName !== "") {
          values[headerIdx] = curr[headerName]
        }
      }
      return values
    }

    function fetchSS(url) {
      try {
        return SpreadsheetApp.openByUrl(url)
      } catch (e) {
        return null
      }
    }
    function fetchDayOfStudySheet(ss) {
      try {
        return ss.getSheetByName(TRACKERDATA.SheetNames.DAYOFSTUDY.Name)
      } catch (e) {
        return null
      }
    }
    function fetchDayOfStudyCounter(url) {
      let fetchDayOfStudyCounter_ = compose(statusCount, fetchStatuses, fetchDayOfStudySheet, fetchSS)
      return fetchDayOfStudyCounter_(url)


      function fetchStatuses(sheet) {

        let countObj
        try {
          let { body } = getDataBySheet(sheet, 2, 3);
          let [fistRowProps, ...restOfRowsProp] = body
          let getValuesByColHeader = (str) => body.map(props => {
            if (["Study Dates", "Number of Participants per Day"].includes(str)) {
              return ""
            }
            return props.hasOwnProperty(str) ? props[str] : `Not Found: ${str}`
          }
          ).filter(str => str !== "")

          return {
            participantType: {
              data: getValuesByColHeader("Participant Type"),
              countObj: initialParticipantStatusCount(getValidationFields(fistRowProps, "Participant Type"))
            },
            attendenceStatus: {
              data: getValuesByColHeader("Attendance status"),
              countObj: initialParticipantStatusCount(getValidationFields(fistRowProps, "Attendance status"))
            }
          }
        } catch (e) {
          return {
            participantType: {
              data: [],
              countObj: TRACKERDATA.SheetNames.ClosedStudies.ParticipantType
            },
            attendenceStatus: {
              data: [],
              countObj: TRACKERDATA.SheetNames.ClosedStudies.AttendenceStatus
            },
          }
        }

        function getValidationFields(props, str) {
          let participantStatusRange = props.getRangeByColHeader(str)
          let dataValidation = participantStatusRange.getDataValidation();
          let [criteriaValues, hasValidation] = dataValidation.getCriteriaValues();
          return hasValidation ? criteriaValues : []
        }
        function initialParticipantStatusCount(arr) {
          return arr.reduce((acc, next) => {
            acc[next] = 0
            return acc
          }, new Object(null))
        }
      }
      function statusCount({ attendenceStatus, participantType }) {
        let data = [...attendenceStatus.data, ...participantType.data]
        let countObj = Object.assign({}, attendenceStatus.countObj, participantType.countObj)
        debugger
        return data.reduce((acc, participantStatus) => {
          acc.hasOwnProperty(participantStatus) ? acc[participantStatus] += 1 : isNaN(Number(acc[participantStatus])) ? acc : acc[participantStatus] = 1
          return acc
        }, countObj)
      }
    }
  }
  // send email notifications
  function getlistOfNewStudies() {
    const sheetName = TRACKERDATA.SheetNames.UnallocatedStudies.Name
    const sheet = activeSpreadsheet.getSheetByName(sheetName)
    let values = sheet.getDataRange().getValues()
    const { headers, getBody } = getDataBySheet(sheet, TRACKERDATA.SheetNames.UnallocatedStudies.headerIdx - 1, TRACKERDATA.SheetNames.UnallocatedStudies.bodyIdx - 1);
    const body = getBody()
    const filterNewUXtendStudies = (props) => ["Header Name Not Found @go/studyqueue", ""].some(str => props["Timestamp Added to RSS Queue"] === str);
    const newUXtendStudies = body.filter(filterNewUXtendStudies);
    const listOfNewStudies = newUXtendStudies.map(props => {
      return {
        studyName: props[["What's the name of the study?"]],
        studyLink: props["Review instructions below, then paste the link to your study spreadsheet"],
        PL: props["PL"],
        studyLocation: props["Where in the world would the participants in your study be located?"],
        tsRange: props.getRangeByColHeader("Timestamp Added to RSS Queue")
      }
    })
    return listOfNewStudies
  }
  function activeStudiesNotification() {
    const sheetName = TRACKERDATA.SheetNames.UnallocatedStudies.Name
    const sheet = activeSpreadsheet.getSheetByName(sheetName)
    let values = sheet.getDataRange().getValues()
    const { headers, getBody } = getDataBySheet(sheet, TRACKERDATA.SheetNames.UnallocatedStudies.headerIdx - 1, TRACKERDATA.SheetNames.UnallocatedStudies.bodyIdx - 1);
    const body = getBody()
    const filterNewUXtendStudies = (props) => ["Header Name Not Found @go/studyqueue", ""].some(str => props["Timestamp Added to RSS Queue"] === str);
    const newUXtendStudies = body.filter(filterNewUXtendStudies);
    const listOfNewStudies = newUXtendStudies.map(props => {
      return {
        studyName: props[["What's the name of the study?"]],
        studyLink: props["Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uer-recruiters@google.com added as an editor/collaborator."],
        PL: props["PL"],
        studyLocation: props["Where in the world would the participants in your study be located?"],
        tsRange: props.getRangeByColHeader("Timestamp Added to RSS Queue")
      }
    })

    // const uxTendHtmlTemplatev2 = getUXtendHtmlTemplatev2(listOfNewStudies)
    // const uxTendTemplate = HtmlService.createTemplateFromFile("UXtendNotification Template").evaluate();

    let htmlBody = getHtmlBodyNotification(listOfNewStudies)
    const tsRangeList = listOfNewStudies.map(({ tsRange }) => tsRange);
    let emailOptions = getEmailOptionsStudyNotification({ htmlBody: htmlBody, tsRangeList })
    sendUxTendNotifications(emailOptions)

  }
  function getHtmlBodyNotification(listOfNewStudies) {
    const htmlBody = HtmlService.createTemplateFromFile("UXtendNotification")
    htmlBody.data = listOfNewStudies
    return htmlBody.evaluate().getContent()
  }
  function getEmailOptionsStudyNotification({ htmlBody, tsRangeList }) {
    let aliases = GmailApp.getAliases();
    let recipient = TRACKERDATA.Notification.recipient
    const defaultSubject = `New study request for UXtend`
    let options = {
      // cc,
      from: aliases.find(str => str === "uxi-alerts@google.com"),
      htmlBody,
      // replyTo
    }

    debugger
    return {
      recipient,
      subject: defaultSubject,
      body: "UXtend - Incoming Study Notification",
      options,
      tsRangeList
    }
  }
  function sendUxTendNotifications({ recipient, subject, body, options, tsRangeList }) {
    debugger
    try {
      GmailApp.sendEmail(recipient, subject, body, options)
    } catch (e) {
      throw (`unable to send UXtendNotification ${e}`)
    }
    updateRowTimestamp()

    function updateRowTimestamp() {
      try {
        tsRangeList.forEach(tsRange => tsRange !== undefined && tsRange.setValue(new Date()))
      } catch (e) {
        tsRangeList.forEach(tsRange !== undefined && tsRange.setValue(e));
        throw (`unable to update timestamp ${e}`)
      }
    }
  }


  function getEmailOptions({ htmlBody, subject, props }) {
    let tsRange = props.getRangeByColHeader("Timestamp Added to RSS Queue");
    let aliases = GmailApp.getAliases();
    let recipient = TRACKERDATA.Notification.recipient
    let options = {
      // cc,
      from: aliases.find(str => str === "uxi-alerts@google.com"),
      htmlBody,
      // replyTo
    }

    debugger
    return {
      recipient,
      subject,
      body: "UXtend - Incoming Study Notification",
      options,
      tsRange
    }
  }
  function getUXtendHtmlTemplatev2(listOfNewStudies) {
    const uxTendTemplate = HtmlService.createTemplateFromFile("UXtendNotification Template").evaluate()
    return uxTendTemplate
  }
  function uxTendHtmlTemplate(props) {
    const liWrapper = attributeWrapper('<li>')
    const internationalStr = "Where in the world would the participants in your study be located?"
    const studyLocation = props.hasOwnProperty(internationalStr) ? props[internationalStr] : ''
    const studyLink = props.hasOwnProperty("Review instructions below, then paste the link to your study spreadsheet") ? props["Review instructions below, then paste the link to your study spreadsheet"] : ""

    const uxTendList = getList(props);
    const doc = DocumentApp.openById(TRACKERDATA.SS.HTMLTEMPLATE.Unallocated.id)
      .getBody()
      .getText()
      .replace("${studyProps}", uxTendList)
      .replace("${studyLink}", studyLink)
      .replace("[UXR]", props.hasOwnProperty("Email Address") ? props["Email Address"] : "N/A")
      ;

    const defaultSubject = `New study request for UXtend`
    const subject = isInternationRequest(studyLocation) ? '[International Request] '.concat(defaultSubject) : defaultSubject

    return {
      htmlBody: doc,
      subject: `${subject}`,
      props
    }
    function isInternationRequest(str) {
      const countryList = str.replace(' and', ', ')
        .replace(';', ',')
        .replace('United States ', '')
        .split(', ')
        .map(str => str.trim())
        .filter(str => str !== "")
      return countryList.length >= 1
    }
    function getList(props) {
      const setHtmlParser = (listName, headerName) => Object.assign({}, { listName, headerName })
      const list = [
        "Submission Timestamp",
        "Timestamp for Moved to Active Tab",
        "UXR",
        "RC",
        "Where in the world would the participants in your study be located?",
        // "Studies Tab
        // "Can requests for participants including any region other than the United States be flagged here? "Maybe a line on the subject line saying “International Request”?
        "Number of PPTs",
        "Study Name",
        "Target Start Date",
        "Study Type",
        "Study Spreadsheet Link ",
        "Researcher location",
        "Product Area",
        "Pod Supporting Study",
      ]
      const list2 = [
        setHtmlParser("Submission Timestamp", "Timestamp"),
        setHtmlParser("Timestamp for Moved to Active Tab", "timestamp for moved to Active tab"),
        setHtmlParser("UXR", "Email Address"),
        setHtmlParser("RC", "RC"),
        setHtmlParser("Where in the world would the participants in your study be located?", "Where in the world would the participants in your study be located?"),
        setHtmlParser("Number of PPTs", "How many participants do you need for this study?"),
        setHtmlParser("Study Name", "What's the name of the study?"),
        setHtmlParser("Target Start Date", "What is the targeted first date of the study?"),
        setHtmlParser("Study Type", "Which type of study are you planning to do?"),
        setHtmlParser("Study Spreadsheet Link", "Review instructions below, then paste the link to your study spreadsheet"),
        setHtmlParser("Researcher location", "Researcher Location"),
        setHtmlParser("Product Area", "Product Area (recruiters do not edit this column)"),
        setHtmlParser("Pod Supporting Study", "Pod"),
      ]

      return list2.map(({ listName, headerName }) => {
        return props.hasOwnProperty(headerName) ? `${listName}: ${props[headerName]}` : `${listName}: N/A`
      }).map(liWrapper).join("")
    }
    function attributeWrapper(element) {
      const openIdx = element.indexOf('<') + 1
      const closedElement = element.slice(0, openIdx).concat('/').concat(element.slice(openIdx));
      return function (str) {
        return `${element}${str}${closedElement}`
      }
    }

  }

}
function getPodLead(podName) {
  try {
    const podNum = podName.replace(/pod\s*/gi, "")
    const sheet = SpreadsheetApp.openById(TRACKERDATA.SS.StaffingTrix.id).getSheetByName(TRACKERDATA.SS.StaffingTrix.Personnel)
    const { body } = getDataBySheet(sheet);
    const selectPodLeads = body.filter(props => props[TRACKERDATA.SS.StaffingTrix.COLHEAD.PODLEAD].includes(podNum))
      .filter(filterActive)
    const ldap = selectPodLeads.map(props => props[TRACKERDATA.SS.StaffingTrix.COLHEAD.ldap])
    debugger
    return ldap.join(', ')
  } catch (e) {
    throw (`Unable to determine pod lead via`)
  }
  function filterActive(props) {
    let status = props.hasOwnProperty("In/Active") ? props["In/Active"] : "Not found"
    let isActive = /^active/gi.test(status)
    return isActive
  }
}
function testpodlead() {
  let str = "Pod 8"
  let pl = getPodLead(str)
  debugger
}