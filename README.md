# How to write a design doc and get it reviewed (before it’s obsolete).

go/writing-a-design-doc

<!--*
# Document freshness: For more information, see go/fresh-source.
freshness: { owner: 'johntobin' reviewed: '2021-04-28' review_interval: '1 year' }
*-->

<!-- Original doc:
https://docs.google.com/a/google.com/open?id=14fBACUn8SfrewfxeuWCdqZXr2wC3i_RSJH2uIu0eizI
-->

**Authors:** douglascox@

**Status:** pending

**Last major update:** 
2021-08-17 - added README.md

**Resources:** 
[UXtend - RSS Queue](https://docs.google.com/spreadsheets/d/1Vkxq0jm_R4Py09BfujFoKC1EWtQJaJqDD30bJl4bqg4/edit?resourcekey=0-JURaiFurHGpRL2rZgh8FdA#gid=568734748)
[UXtend Tab Template](https://docs.google.com/spreadsheets/d/1ZpTgyuR0EFBLWiK-0mEyvq-BtYu0ikvkWfUa30EQf9U/edit#gid=1434907922) - used by script to copy over "Day-Of Study" sheet. different version copy over different sheets. were version as of 8.17.21 used "PptTmz Day-Of Study"
[[Google] Orion Study Spreadsheet Template (go/orion-study-sheet)](https://docs.google.com/spreadsheets/d/1swcx4NzjriKpJWAE-2CnqkKJ0R2p5KkHZBhmcZLucAI/edit#gid=1054252732) - consist mainly of Project Orion code, however the creation of "Day-Of Study" sheet exist here
[UXtend Tracker - Automation Requirements](https://docs.google.com/document/d/1E3-CyjtqYCf51cZA2wEVhh5VQFgqn7seDjV0g5UHLOw/edit?resourcekey=0-nfgtAoQgALRLEIeTjYM-Iw#heading=h.vj7rlii3205l) - this was the original document, the current version has deviated from these in various ways.
**Contents**

[TOC]

*Notes:*

*   Currently 2018-08-17 this library contains code relative to two different application calls. One is for the "UXtend - RSS Queue" and the other "go/orion-study-sheet". Both as it relates to UXtend project are in a single library, as a result when an update is made to either trix the library version is updated for both. This reflects a dispariety of versioning relative to either trix. 


## Objective {#objective}

* Provide automated script to help manage UXtend program

## Background {#background}

*came from jbarnowski@, nocella@*

## Overview {#overview}

*"UXtend - RSS Queue" populates studies marked for UXtend. This is accomplished via a push from the go/studyqueue, and a pull from the "UXtend - RSS Queue" incase any gets missed from the push. From here' there's functionality to notifiy that studies have been added to the sheet name "Unallocated Studies". When a study get moved through the UXtend process, a notification is sent to the UXtend memeber and RC when it is claimed, and the study get moved to sheet named "UXtend Active Studies", the following fields then get populated "Remote Tool", "Timestamp Moved to Active". After a study is closed the following are populated "Timestamp Moved to Closed", with the following pulling data form the study's trix "Day-Of Study" sheet "Scheduled",	"Rescheduled", "Backup", "Attended", "Cancelled", "Cancelled by UXR", "No-show".*
*[Google] Orion Study Spreadsheet Template (go/orion-study-sheet)  - this trix implements code from the library to create the "Day-Of Study" sheet as well as update this sheet with any participant addition to the "Schedule" sheet*


## Infrastructure {#infrastructure}

*UXtendTab.gs contains the logic for the [[Google] Orion Study Spreadsheet Template (go/orion-study-sheet)](https://docs.google.com/spreadsheets/d/1swcx4NzjriKpJWAE-2CnqkKJ0R2p5KkHZBhmcZLucAI/edit#gid=1054252732) operations* This file has two main function which get used. fns createdUXtendTab, updateUXtendTab

*UXtendTracker.gs contains the logic for the [UXtend - RSS Queue](https://docs.google.com/spreadsheets/d/1Vkxq0jm_R4Py09BfujFoKC1EWtQJaJqDD30bJl4bqg4/edit?resourcekey=0-JURaiFurHGpRL2rZgh8FdA#gid=568734748)* This file has four main function which get used. fns claimUnallocatedStudies, closeActiveStudies, activeStudiesNotification, generateWeeklyHeatMap

## Detailed Design {#design}

*As these function calls are made on a spreadsheet referencing the code/library the function used must exist in the global name space. Generally i've worked to have the spreadsheet calling these function the pass itself and the Browser object through.*

## Project Information {#project}

*Please provide the following information about the project:*

external Links
* [UXtend HTML template](https://docs.google.com/document/d/198M3_9aGeHs46uFr2DygwolUWxKB3DRgvcP7nSEufy8/edit?resourcekey=0-9Lh_78SnR_oo4gd7a1I5WA)
* [UXtend Tab Template](https://docs.google.com/spreadsheets/d/1ZpTgyuR0EFBLWiK-0mEyvq-BtYu0ikvkWfUa30EQf9U/edit#gid=1434907922)
* [Staffing Trix](https://docs.google.com/spreadsheets/d/1ZoGLcVi_s7rZQgUx-Gan8rwK89yTTQsbuJm1BqTF1Vc/edit#gid=85027790)

## Dependency Considerations {#dependencies}

*As code within this library contains function for the study template and the uxtend tracker, are difficult to determine which trix requires it. There's a move to have the functionality seperated for each trix into there own library. This would been any updates for UXtend Tracker or Study Template won't impact the other trixes versions.*

## Document History {#document_history}

**Date** | **Author** | **Description** | **Reviewed by** | **Signed off by**
-------- | ---------- | --------------- | --------------- | -----------------
20210817      | douglascox@        | setup README | ...             | ...
...      | ...        | Dummy entry for | ...             | ...
